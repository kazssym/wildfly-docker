# Supported tags and respective `Dockerfile` links

  - [`14.0.0.Final`, `14.0`, `14`, `latest` (13/Dockerfile)](https://bitbucket.org/kazssym/wildfly-docker/src/master/13/Dockerfile)
  - [`13.0.0.Final`, `13.0`, `13` (13/Dockerfile)](https://bitbucket.org/kazssym/wildfly-docker/src/master/13/Dockerfile)
  - [`12.0.0.Final`, `12.0`, `12` (12/Dockerfile)](https://bitbucket.org/kazssym/wildfly-docker/src/master/12/Dockerfile)
  - [`11.0.0.Final`, `11.0`, `11` (11/Dockerfile)](https://bitbucket.org/kazssym/wildfly-docker/src/master/11/Dockerfile)
  - [`10.1.0.Final`, `10.1`, `10` (10/Dockerfile)](https://bitbucket.org/kazssym/wildfly-docker/src/master/10/Dockerfile)

**Note** builds are no longer automated.

[1]: http://www.wildfly.org/
