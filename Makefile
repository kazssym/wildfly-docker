# Makefile
# Copyright (C) 2018-2019 Kaz Nishimura
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

SUBDIRS = 11 14 15 16

all build push:
	for i in $(SUBDIRS); do \
	  (cd $$i && $(MAKE) $@) || exit 1; \
	done
