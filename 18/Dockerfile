# Dockerfile for WildFly 18
# Copyright (C) 2017-2020 Kaz Nishimura
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

ARG JDK=adoptopenjdk:8-jdk-openj9
FROM $JDK

WORKDIR /opt/jboss/wildfly

RUN set -x && \
useradd -r -u 999 -d /var/opt/jboss/wildfly wildfly && \
id wildfly && \
mkdir -p ~wildfly && \
chown -R wildfly: ~wildfly

ARG WILDFLY_VERSION=18.0.1.Final
RUN set -x && \
curl -OL https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz && \
gzip -dc wildfly-$WILDFLY_VERSION.tar.gz | tar -x --strip-components=1 --no-same-owner && \
rm -f wildfly-$WILDFLY_VERSION.tar.gz && \
chown -R wildfly: standalone domain

ENV OPENJ9_JAVA_OPTIONS="-Xtune:virtualized -Xshareclasses"
ENV LAUNCH_JBOSS_IN_BACKGROUND=1
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b=0.0.0.0"]

USER wildfly
